import json
import datetime
from models import app, db, Game, Company, Platform, Age_Rating, Genre, Version
from enum import Enum

class Age_rating_enum(Enum):
	Three = 1
	Seven = 2
	Twelve = 3
	Sixteen = 4
	Eighteen = 5
	RP = 6
	EC = 7
	E = 8
	E10 = 9
	T = 10
	M = 11
	AO = 12

class Age_rating_category_enum(Enum):
	ESRB = 1
	PEGI = 2

class Platform_category_enum(Enum):
	Console = 1
	Arcade = 2
	Platform = 3
	Operating_System = 4
	Portable_Console = 5
	Computer = 6

def load_json(filename):
    with open(filename, encoding='utf8') as file:
        jsn = json.load(file)
        file.close()
    
    return jsn

def create_db():
	# Create the age ratings
	age_ratings = load_json('API/Age_Rating.json')
	
	for age_rating in age_ratings:
		id = age_rating['id']
		rating = Age_rating_enum(age_rating['rating']).name
		category = Age_rating_category_enum(age_rating['category']).name
		cover_url = None
		if 'rating_cover_url' in age_rating:
			cover_url = age_rating['rating_cover_url']

		ratingObject = Age_Rating(id = id, rating = rating, category = category,
			cover_url = cover_url)
		checkExisting = Age_Rating.query.filter_by(id = ratingObject.id).first()
		if (checkExisting == None):
			db.session.add(ratingObject)
			db.session.commit()
	
	# Create the genres
	genres = load_json('API/Genres.json')

	for genre in genres:
		id = genre['id']
		name = genre['name']

		genreObject = Genre(id = id, name = name)
		checkExisting = Genre.query.filter_by(id = genreObject.id).first()
		if (checkExisting == None):
			db.session.add(genreObject)
			db.session.commit()

    # Create the games
	games = load_json('API/Game_Titles.json')
	covers = load_json('API/Cover.json')
    
	for game in games:
		id = game['id']
		name = game['name']
		summary = None
		if 'summary' in game:
			summary = game['summary']
		rating = None
		if 'rating' in game:
			rating = str(round(game['rating'],2))
		first_release_date = None
		if 'first_release_date' in game:
			try:
				date = datetime.datetime.fromtimestamp(game['first_release_date'])
				first_release_date = date.strftime('%Y-%m-%d')
			except:
				pass
		cover_url = None
		if 'cover' in game:
			for cover in covers:
				if cover['id'] == game['cover']:
					cover_url = cover['url']
					break
		
		gameObject = Game(id = id, name = name, summary = summary, rating = rating,
			first_release_date = first_release_date, cover_url = cover_url)
		checkExisting = Game.query.filter_by(id = gameObject.id).first()
		if (checkExisting == None):
			db.session.add(gameObject)
			db.session.commit()

			if 'age_ratings' in game:
				for age_rating_id in game['age_ratings']:
					ar = Age_Rating.query.filter_by(id = age_rating_id).first()
					if ar != None:
						result = ""
						for i in range(len(gameObject.rated)):
							if (gameObject.rated[i].id == age_rating_id):
								result = "Existing"
						if (result == ""):
							gameObject.rated.append(ar)
							db.session.commit()
			if 'genres' in game:
				for genre_id in game['genres']:
					genreObject = Genre.query.filter_by(id = genre_id).first()
					if genreObject != None:
						result = ""
						for i in range(len(gameObject.belongsTo)):
							if (gameObject.belongsTo[i].id == genre_id):
								result = "Existing"
						if (result == ""):
							gameObject.belongsTo.append(genreObject)
							db.session.commit()
	
	# Create the parent companies first
	p_companies = load_json('API/Parent_Company.json')
	c_websites = load_json('API/Company_Website.json')
	c_logos = load_json('API/Company_Logos.json')

	for company in p_companies:
		if Company.query.filter_by(id = company['id']).first() == None:
			id = company['id']
			name = company['name']
			description = None
			if 'description' in company:
				description = company['description']
			parent_id = None
			if 'parent' in company:
				parent_id = company['parent']
			start_date = None
			if 'start_date' in company:
				try:
					date = datetime.datetime.fromtimestamp(company['start_date'])
					start_date = date.strftime('%Y-%m-%d')
				except:
					pass
			url = None
			if 'websites' in company:
				for website in c_websites:
					if website['id'] == company['websites'][0]:
						url = website['url']
						break
			logo_url = None
			if 'logo' in company:
				for logo in c_logos:
					if logo['id'] == company['logo']:
						logo_url = logo['url']
						break
			
			companyObject = Company(id = id, name = name, description = description,
				start_date = start_date, url = url, logo_url = logo_url)
			checkExisting = Company.query.filter_by(id = companyObject.id).first()
			if (checkExisting == None):
				db.session.add(companyObject)
				db.session.commit()
				# Create the game-company relationships
				if 'developed' in company:
					for game_id in company['developed']:
						gameObject = Game.query.filter_by(id = game_id).first()
						if gameObject != None:
							result = ""
							for i in range(len(companyObject.created)):
								if (companyObject.created[i].id == game_id):
									result = "Existing"
							if (result == ""):
								companyObject.created.append(gameObject)
								db.session.commit()
				if 'published' in company:
					for game_id in company['published']:
						gameObject = Game.query.filter_by(id = game_id).first()
						if gameObject != None:
							result = ""
							for i in range(len(companyObject.created)):
								if (companyObject.created[i].id == game_id):
									result = "Existing"
							if (result == ""):
								companyObject.created.append(gameObject)
								db.session.commit()
	
	# Create the children companies
	companies = load_json('API/Companies.json')

	for company in companies:
		id = company['id']
		name = company['name']
		description = None
		if 'description' in company:
			description = company['description']
		parent_id = None
		if 'parent' in company:
			parent_id = company['parent']
		start_date = None
		if 'start_date' in company:
			try:
				date = datetime.datetime.fromtimestamp(company['start_date'])
				start_date = date.strftime('%Y-%m-%d')
			except:
				pass
		url = None
		if 'websites' in company:
			for website in c_websites:
				if website['id'] == company['websites'][0]:
					url = website['url']
					break
		logo_url = None
		if 'logo' in company:
			for logo in c_logos:
				if logo['id'] == company['logo']:
					logo_url = logo['url']
					break
		
		companyObject = Company(id = id, name = name, description = description,
			start_date = start_date, url = url, logo_url = logo_url)
		checkExisting = Company.query.filter_by(id = companyObject.id).first()
		if (checkExisting == None):
			if (parent_id != None):
				parentCompanyObject = Company.query.filter_by(id = parent_id).first()
				if (parentCompanyObject != None and companyObject.parent == None):
					companyObject.parent = parentCompanyObject
			db.session.add(companyObject)
			db.session.commit()
			# Create the game-company relationship
			if 'developed' in company:
				for game_id in company['developed']:
					gameObject = Game.query.filter_by(id = game_id).first()
					if gameObject != None:
						result = ""
						for i in range(len(companyObject.created)):
							if (companyObject.created[i].id == game_id):
								result = "Existing"
						if (result == ""):
							companyObject.created.append(gameObject)
							db.session.commit()
			if 'published' in company:
				for game_id in company['published']:
					gameObject = Game.query.filter_by(id = game_id).first()
					if gameObject != None:
						result = ""
						for i in range(len(companyObject.created)):
							if (companyObject.created[i].id == game_id):
								result = "Existing"
						if (result == ""):
							companyObject.created.append(gameObject)
							db.session.commit()

	# Check for missing game-company relationships
	i_companies = load_json('API/Involved_Companies.json')
	for game in games:
		gameObject = Game.query.filter_by(id = game['id']).first()
		if 'involved_companies' in game:
			for i_company_id in game['involved_companies']:
				for i_company in i_companies:
					if (i_company['id'] == i_company_id):
						companyObject = Company.query.filter_by(id = i_company['company']).first()
						if companyObject not in gameObject.companies and companyObject != None:
							companyObject.created.append(gameObject)
							db.session.commit()

    # Create the platform versions
	versions = load_json('API/Platform_Versions.json')
	release_dates = load_json('API/Platform_Version_Release_Dates.json')

	for version in versions:
		id = version['id']
		name = version['name']
		release_date = None
		if 'platform_version_release_dates' in version:
			for rd in release_dates:
				if rd['id'] == version['platform_version_release_dates'][0]:
					release_date = rd['human']
					break
	
		versionObject = Version(id = id, name = name, release_date = release_date)
		checkExisting = Version.query.filter_by(id = versionObject.id).first()
		if (checkExisting == None):
			db.session.add(versionObject)
			db.session.commit()

	# Create the platforms
	platforms = load_json('API/Platforms.json')
	p_websites = load_json('API/Platform_Websites.json')
	p_logos = load_json('API/Platform_Logos.json')

	for platform in platforms:
		id = platform['id']
		name = platform['name']
		summary = None
		if 'summary' in platform:
			summary = platform['summary']
		category = None
		if 'category' in platform:
			category = Platform_category_enum(platform['category']).name
		url = None
		if 'websites' in platform:
			for website in p_websites:
				if website['id'] == platform['websites'][0]:
					url = website['url']
					break
		logo_url = None
		if 'platform_logo' in platform:
			for logo in p_logos:
				if logo['id'] == platform['platform_logo']:
					logo_url = logo['url']
					break
		
		platformObject = Platform(id = id, name = name, summary = summary,
			category = category, url = url, logo_url = logo_url)
		checkExisting = Platform.query.filter_by(id = platformObject.id).first()
		if (checkExisting == None):
			db.session.add(platformObject)
			db.session.commit()

			for version_id in platform['versions']:
				versionObject = Version.query.filter_by(id = version_id).first()
				if versionObject != None:
					result = ""
					for i in range(len(platformObject.versions)):
						if (platformObject.versions[i].id == version_id):
							result = "Existing"
					if (result == ""):
						platformObject.versions.append(versionObject)
						db.session.commit()

    # Create the game-platform relationships
	for game in games:
		gameObject = Game.query.filter_by(id = game['id']).first()
		if 'platforms' in game:
			for platform_id in game['platforms']:
				platformObject = Platform.query.filter_by(id = platform_id).first()
				if platformObject != None:
					result = ""
					for i in range(len(platformObject.games)):
						if (platformObject.games[i].id == gameObject.id):
							result = "Existing"
					if (result == ""):
						gameObject.availableIn.append(platformObject)
						db.session.commit()

	# Create the company-platform relationships
	for company in companies:
		companyObject = Company.query.filter_by(id = company['id']).first()
		if 'developed' in company:
			for game_id in company['developed']:
				gameObject = Game.query.filter_by(id = game_id).first()
				if gameObject != None:
					platformObjectList = gameObject.availableIn
					for platformObject in platformObjectList:
						if platformObject not in companyObject.hasDevelopedIn:
							companyObject.hasDevelopedIn.append(platformObject)
							db.session.commit()
		if 'published' in company:
			for game_id in company['published']:
				gameObject = Game.query.filter_by(id = game_id).first()
				if gameObject != None:
					platformObjectList = gameObject.availableIn
					for platformObject in platformObjectList:
						if platformObject not in companyObject.hasDevelopedIn:
							companyObject.hasDevelopedIn.append(platformObject)
							db.session.commit()
	
	for company in p_companies:
		companyObject = Company.query.filter_by(id = company['id']).first()
		if 'developed' in company:
			for game_id in company['developed']:
				gameObject = Game.query.filter_by(id = game_id).first()
				if gameObject != None:
					platformObjectList = gameObject.availableIn
					for platformObject in platformObjectList:
						if platformObject not in companyObject.hasDevelopedIn:
							companyObject.hasDevelopedIn.append(platformObject)
							db.session.commit()
		if 'published' in company:
			for game_id in company['published']:
				gameObject = Game.query.filter_by(id = game_id).first()
				if gameObject != None:
					platformObjectList = gameObject.availableIn
					for platformObject in platformObjectList:
						if platformObject not in companyObject.hasDevelopedIn:
							companyObject.hasDevelopedIn.append(platformObject)
							db.session.commit()
	
	# Check for missing company-platform relationships
	for game in games:
		gameObject = Game.query.filter_by(id = game['id']).first()
		if 'involved_companies' in game:
			for i_company_id in game['involved_companies']:
				for i_company in i_companies:
					if (i_company['id'] == i_company_id):
						companyObject = Company.query.filter_by(id = i_company['company']).first()
						if companyObject != None:
							for platformObject in gameObject.availableIn:
								if platformObject not in companyObject.hasDevelopedIn:
									companyObject.hasDevelopedIn.append(platformObject)
									db.session.commit()

create_db()