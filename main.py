from flask import Flask, render_template, request, redirect, url_for, json, Response
from create_db import app, db, Game, Company, Platform, Age_Rating, Genre, Version, create_db
from sqlalchemy.sql import func
from sqlalchemy.orm import aliased
from sqlalchemy import desc
from tests import DBTestCases
import requests
import unittest
import math
import tests

@app.after_request
def add_header(response):
    response.cache_control.max_age = 30
    return response

@app.route('/')
def index():
	return render_template('index.html')

@app.route('/games/<queryID>', methods=['GET','POST'])
def show_games(queryID):
	if (queryID != None):
		if request.method == 'POST':
			qID = request.form.get('sort_order')
		else:
			qID = queryID

		if (qID == "0"):
			_games = db.session.query(Game).all()
		elif (qID == "00"):
			_games = db.session.query(Game).order_by(Game.name).all()
		elif (qID == "10"):
			_games = db.session.query(Game).order_by(Game.first_release_date).all()
		elif (qID == "20"):
			_games = db.session.query(Game).order_by(Game.rating).all()
		elif (qID == "30"):
			_games = db.session.query(Game).join(Game.belongsTo).group_by(Game.id).order_by(func.min(Genre.name),func.max(Genre.name)).all()
		elif (qID == "40"):
			_games = db.session.query(Game).join(Game.rated).group_by(Game.id).order_by(func.min(Age_Rating.rating),func.max(Age_Rating.rating)).all()
		elif (qID == "50"):
			_games = db.session.query(Game).join(Game.availableIn).group_by(Game.id).order_by(func.min(Platform.name),func.max(Platform.name)).all()
		elif (qID == "60"):
			_games = db.session.query(Game).join(Game.companies).group_by(Game.id).order_by(func.min(Company.name),func.max(Company.name)).all()
		elif (qID == "01"):
			_games = db.session.query(Game).order_by(Game.name.desc()).all()
		elif (qID == "11"):
			_games = db.session.query(Game).order_by(Game.first_release_date.desc()).all()
		elif (qID == "21"):
			_games = db.session.query(Game).order_by(Game.rating.desc()).all()
		elif (qID == "31"):
			_games = db.session.query(Game).join(Game.belongsTo).group_by(Game.id).order_by(desc(func.max(Genre.name)),desc(func.min(Genre.name))).all()
		elif (qID == "41"):
			_games = db.session.query(Game).join(Game.rated).group_by(Game.id).order_by(desc(func.max(Age_Rating.rating)),desc(func.min(Age_Rating.rating))).all()
		elif (qID == "51"):
			_games = db.session.query(Game).join(Game.availableIn).group_by(Game.id).order_by(desc(func.max(Platform.name)),desc(func.min(Platform.name))).all()
		elif (qID == "61"):
			_games = db.session.query(Game).join(Game.companies).group_by(Game.id).order_by(desc(func.max(Company.name)),desc(func.min(Company.name))).all()

		pgnum = request.args.get("page", 1, type = int)
		current = (pgnum - 1) * 20
		limit = pgnum * 20
		maxpages = math.ceil(len(_games) / 20)
		pagegames = []
		while(current < limit and current < len(_games)):
			pagegames.append(_games[current])
			current += 1
		return render_template('show_games.html', queryID = qID, pgnum = pgnum, pagegames = pagegames, maxpages = maxpages)

@app.route('/game/<game_id>')
def show_game(game_id):
	if (game_id != None):
		game = Game.query.filter_by(id = game_id).first()
		return render_template('game.html', game = game)

@app.route('/platforms/<queryID>', methods=['GET','POST'])
def show_platforms(queryID):
	if (queryID != None):
		if request.method == 'POST':
			qID = request.form.get('sort_order')
		else:
			qID = queryID

		if (qID == "0"):
			_platforms = db.session.query(Platform).all()
		elif (qID == "00"):
			_platforms = db.session.query(Platform).order_by(Platform.name).all()
		elif (qID == "10"):
			_platforms = db.session.query(Platform).order_by(Platform.url).all()
		elif (qID == "20"):
			_platforms = db.session.query(Platform).order_by(Platform.category).all()
		elif (qID == "30"):
			_platforms = db.session.query(Platform).order_by(Platform.summary).all()
		elif (qID == "40"):
			_platforms = db.session.query(Platform).join(Platform.versions).group_by(Platform.id).order_by(func.min(Version.release_date),func.max(Version.release_date)).all()
		elif (qID == "50"):
			_platforms = db.session.query(Platform).join(Platform.games).group_by(Platform.id).order_by(func.min(Game.name),func.max(Game.name)).all()
		elif (qID == "60"):
			_platforms = db.session.query(Platform).join(Platform.companies).group_by(Platform.id).order_by(func.min(Company.name),func.max(Company.name)).all()
		elif (qID == "01"):
			_platforms = db.session.query(Platform).order_by(Platform.name.desc()).all()
		elif (qID == "11"):
			_platforms = db.session.query(Platform).order_by(Platform.url.desc()).all()
		elif (qID == "21"):
			_platforms = db.session.query(Platform).order_by(Platform.category.desc()).all()
		elif (qID == "31"):
			_platforms = db.session.query(Platform).order_by(Platform.summary.desc()).all()
		elif (qID == "41"):
			_platforms = db.session.query(Platform).join(Platform.versions).group_by(Platform.id).order_by(desc(func.max(Version.release_date)),desc(func.min(Version.release_date))).all()
		elif (qID == "51"):
			_platforms = db.session.query(Platform).join(Platform.games).group_by(Platform.id).order_by(desc(func.max(Game.name)),desc(func.min(Game.name))).all()
		elif (qID == "61"):
			_platforms = db.session.query(Platform).join(Platform.companies).group_by(Platform.id).order_by(desc(func.max(Company.name)),desc(func.min(Company.name))).all()

		pgnum = request.args.get("page", 1, type = int)
		current = (pgnum - 1) * 20
		limit = pgnum * 20
		maxpages = math.ceil(len(_platforms) / 20)
		pageplats = []
		while(current < limit and current < len(_platforms)):
			pageplats.append(_platforms[current])
			current += 1
		return render_template('show_platforms.html', queryID = qID, pgnum = pgnum, pageplats = pageplats, maxpages = maxpages)

@app.route('/platform/<platform_id>')
def show_platform(platform_id):
	if (platform_id != None):
		platform = Platform.query.filter_by(id = platform_id).first()
		return render_template('platform.html', platform = platform)

@app.route('/companies/<queryID>', methods=['GET','POST'])
def show_companies(queryID):
	if (queryID != None):
		if request.method == 'POST':
			qID = request.form.get('sort_order')
		else:
			qID = queryID
		print(queryID)
		if (qID == "0"):
			_companies = db.session.query(Company).all()
		elif (qID == "00"):
			_companies = db.session.query(Company).order_by(Company.name).all()
		elif (qID == "10"):
			_companies = db.session.query(Company).order_by(Company.start_date).all()
		elif (qID == "20"):
			_companies = db.session.query(Company).order_by(Company.url).all()
		elif (qID == "30"):
			_companies = db.session.query(Company).order_by(Company.description).all()
		elif (qID == "40"):
			parentAlias = aliased(Company,name='parentCompany')
			_companies = db.session.query(Company).join(parentAlias,Company.parent_id==parentAlias.id, isouter=True).order_by(parentAlias.name).all()
		elif (qID == "50"):
			_companies = db.session.query(Company).join(Company.created).group_by(Company.id).order_by(func.min(Game.name),func.max(Game.name)).all()
		elif (qID == "60"):
			_companies = db.session.query(Company).join(Company.hasDevelopedIn).group_by(Company.id).order_by(func.min(Platform.name),func.max(Platform.name)).all()
		elif (qID == "01"):
			_companies = db.session.query(Company).order_by(Company.name.desc()).all()
		elif (qID == "11"):
			_companies = db.session.query(Company).order_by(Company.start_date.desc()).all()
		elif (qID == "21"):
			_companies = db.session.query(Company).order_by(Company.url.desc()).all()
		elif (qID == "31"):
			_companies = db.session.query(Company).order_by(Company.description.desc()).all()
		elif (qID == "41"):
			parentAlias = aliased(Company,name='parentCompany')
			_companies = db.session.query(Company).join(parentAlias,Company.parent_id==parentAlias.id, isouter=True).order_by(parentAlias.name.desc()).all()
		elif (qID == "51"):
			_companies = db.session.query(Company).join(Company.created).group_by(Company.id).order_by(desc(func.max(Game.name)),desc(func.min(Game.name))).all()
		elif (qID == "61"):
			_companies = db.session.query(Company).join(Company.hasDevelopedIn).group_by(Company.id).order_by(desc(func.max(Platform.name)),desc(func.min(Platform.name))).all()

		pgnum = request.args.get("page", 1, type = int)
		current = (pgnum - 1) * 20
		limit = pgnum * 20
		maxpages = math.ceil(len(_companies) / 20)
		
		pagecomps = []
		while(current < limit and current < len(_companies)):
			pagecomps.append(_companies[current])
			current += 1
		return render_template('show_companies.html', queryID = qID, pgnum = pgnum, pagecomps = pagecomps, maxpages = maxpages)

@app.route('/company/<company_id>')
def show_company(company_id):
	if (company_id != None):
		company = Company.query.filter_by(id = company_id).first()
		return render_template('company.html', company = company)

@app.route('/about', methods = ["GET", "POST"])
def show_about():
	
	project_id = str(16589781)

	c_url1 = 'https://gitlab.com/api/v4/projects/' + project_id + '/repository/commits?all=True&per_page=100&page=1'
	c_url2 = 'https://gitlab.com/api/v4/projects/' + project_id + '/repository/commits?all=True&per_page=100&page=2'
	i_url = 'https://gitlab.com/api/v4/projects/' + project_id + '/issues?per_page=70'

	c_response1 = requests.get(c_url1)
	commits1 = c_response1.json()
	c_response2 = requests.get(c_url2)
	commits2 = c_response2.json()
	
	i_response = requests.get(i_url)
	issues = i_response.json()

	#Hamza Sait, Jessica Hairston, Chu Nguyen, Patrick Lawrence, ara2798
	commitsPerUser = [0,0,0,0,0]
	totalCommits = 0
	for commit in commits1:
		if (commit['author_name'] == "Hamza Sait"):
			commitsPerUser[0] += 1
		elif (commit['author_name'] == "Jessica Hairston"):
			commitsPerUser[1] += 1
		elif (commit['author_name'] == "Chu Nguyen"):
			commitsPerUser[2] += 1
		elif (commit['author_name'] == "Patrick Lawrence"):
			commitsPerUser[3] += 1
		elif (commit['author_name'] == "ara2798"):
			commitsPerUser[4] += 1
		totalCommits += 1
	for commit in commits2:
		if (commit['author_name'] == "Hamza Sait"):
			commitsPerUser[0] += 1
		elif (commit['author_name'] == "Jessica Hairston"):
			commitsPerUser[1] += 1
		elif (commit['author_name'] == "Chu Nguyen"):
			commitsPerUser[2] += 1
		elif (commit['author_name'] == "Patrick Lawrence"):
			commitsPerUser[3] += 1
		elif (commit['author_name'] == "ara2798"):
			commitsPerUser[4] += 1
		totalCommits += 1
	
	issuesPerUser = [0,0,0,0,0]
	totalIssues = 0
	for issue in issues:
		if (issue['assignees'][0]['username'] == "hamzsait"):
			issuesPerUser[0] += 1
		elif (issue['assignees'][0]['username'] == "drfluffingtons"):
			issuesPerUser[1] += 1
		elif (issue['assignees'][0]['username'] == "chu.nguyen"):
			issuesPerUser[2] += 1
		elif (issue['assignees'][0]['username'] == "p.lawrence"):
			issuesPerUser[3] += 1
		elif (issue['assignees'][0]['username'] == "ara2798"):
			issuesPerUser[4] += 1
		totalIssues += 1

	testing = ""
	if request.method == "POST":
		testing = unittest.TestLoader().loadTestsFromModule(tests)
		testing = unittest.TextTestRunner(verbosity=2).run(testing)
	return render_template('About_Us.html', testing = testing, commitsPerUser = commitsPerUser, totalCommits = totalCommits, issuesPerUser = issuesPerUser, totalIssues = totalIssues)

@app.route('/search', methods=['GET','POST'])
def show_search():
	if (request.method == 'POST'):	
		displayResults = True
		
		search = request.form.get("search_box")
		searchReq = '%' + search + '%'

		_games = Game.query.filter(Game.name.like(searchReq)).all()
		_games += Game.query.filter(Game.first_release_date.like(searchReq)).all()
		_games += Game.query.filter(Game.rating.like(searchReq)).all()
		_games += db.session.query(Game).join(Game.belongsTo).filter(Genre.name.like(searchReq)).all()
		_games += db.session.query(Game).join(Game.rated).filter(Age_Rating.rating.like(searchReq)).all()
		_games += db.session.query(Game).join(Game.rated).filter(Age_Rating.category.like(searchReq)).all()

		_platforms = Platform.query.filter(Platform.name.like(searchReq)).all()
		#_platforms += Platform.query.filter(Platform.url.like(searchReq)).all()
		_platforms += Platform.query.filter(Platform.category.like(searchReq)).all()
		#_platforms += Platform.query.filter(Platform.summary.like(searchReq)).all()
		_platforms += db.session.query(Platform).join(Platform.versions).filter(Version.name.like(searchReq)).all()
		_platforms += db.session.query(Platform).join(Platform.versions).filter(Version.release_date.like(searchReq)).all()
		
		_companies = Company.query.filter(Company.name.like(searchReq)).all()
		_companies += Company.query.filter(Company.start_date.like(searchReq)).all()
		#_companies += Company.query.filter(Company.url.like(searchReq)).all()
		#_companies += Company.query.filter(Company.description.like(searchReq)).all()
	
	else:
		displayResults = False
		_games = []
		_platforms = []
		_companies = []

	return render_template('search.html', pagegames = _games, pageplats=_platforms, pagecomps = _companies, displayResults = displayResults)

if __name__ == "__main__":
	app.run()
