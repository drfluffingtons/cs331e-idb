from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('DB_STRING','postgres://postgres:videojuegos@localhost:5432/videojuegosdb')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)

created = db.Table('created',
	db.Column('company_id', db.Integer, db.ForeignKey('company.id')),
	db.Column('game_id', db.Integer, db.ForeignKey('game.id'))
	)

available_in = db.Table('available_in',
	db.Column('game_id', db.Integer, db.ForeignKey('game.id')),
	db.Column('platform_id', db.Integer, db.ForeignKey('platform.id'))
	)

has_developed_in = db.Table('has_developed_in',
	db.Column('company_id', db.Integer, db.ForeignKey('company.id')),
	db.Column('platform_id', db.Integer, db.ForeignKey('platform.id'))
	)

rated = db.Table('rated',
	db.Column('game_id', db.Integer, db.ForeignKey('game.id')),
	db.Column('age_rating_id', db.Integer, db.ForeignKey('age_rating.id'))
	)

belongs_to_genre = db.Table('belongs_to_genre',
	db.Column('game_id', db.Integer, db.ForeignKey('game.id')),
	db.Column('genre_id', db.Integer, db.ForeignKey('genre.id'))
	)

class Game(db.Model):
	__tablename__ = 'game'

	id = db.Column(db.Integer, primary_key = True)
	name = db.Column(db.String(80), nullable = False)
	summary = db.Column(db.String, nullable = True)
	rating = db.Column(db.String, nullable = True)
	first_release_date = db.Column(db.String, nullable = True)
	cover_url = db.Column(db.String, nullable = True)

	companies = db.relationship('Company', secondary = 'created', backref = 'created')

class Age_Rating(db.Model):
	__tablename__ = 'age_rating'

	id = db.Column(db.Integer, primary_key = True)
	rating = db.Column(db.String(10), nullable = False)
	category = db.Column(db.String(5), nullable = False)
	cover_url = db.Column(db.String, nullable = True)

	games = db.relationship('Game', secondary = 'rated', backref = 'rated')

class Genre(db.Model):
	__tablename__ = 'genre'

	id = db.Column(db.Integer, primary_key = True)
	name = db.Column(db.String(80), nullable = False)

	games = db.relationship('Game', secondary = 'belongs_to_genre', backref = 'belongsTo')

class Company(db.Model):
	__tablename__ = 'company'

	id = db.Column(db.Integer, primary_key = True)
	name = db.Column(db.String(50), nullable = False)
	description = db.Column(db.String, nullable = True)
	start_date = db.Column(db.String, nullable = True)
	url = db.Column(db.String, nullable = True) # Checking only first website.  Might need to change
	logo_url = db.Column(db.String, nullable = True)

	parent_id = db.Column(db.Integer, db.ForeignKey("company.id"))
	parent = db.relationship('Company', uselist = False, remote_side=[id])

class Platform(db.Model):
	__tablename__ = 'platform'

	id = db.Column(db.Integer, primary_key = True)
	name = db.Column(db.String(50), nullable = False)
	summary = db.Column(db.String, nullable = True)
	category = db.Column(db.String(20), nullable = True)
	url = db.Column(db.String, nullable = True)
	logo_url = db.Column(db.String, nullable = True)

	versions = db.relationship('Version', backref = 'platform')
	games = db.relationship('Game', secondary = 'available_in', backref = 'availableIn')
	companies = db.relationship('Company', secondary = 'has_developed_in', backref = 'hasDevelopedIn')

class Version(db.Model):
	__tablename__ = 'version'

	id = db.Column(db.Integer, primary_key = True)
	name = db.Column(db.String(50), nullable = False)
	release_date = db.Column(db.String, nullable = True)

	platform_id = db.Column(db.Integer, db.ForeignKey('platform.id'))

#db.drop_all()
db.create_all()
