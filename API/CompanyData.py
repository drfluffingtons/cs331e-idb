import json
import datetime
from csv import writer

def collectLogo():

    with open('Companies.json', 'r') as r:
        pData = json.load(r)

        logos = []
        for x in pData:
            if 'logo' in x:
                logos.append(x['logo'])
        print(logos)

def collectWebsites():

    with open('Companies.json', 'r') as r:
        wData = json.load(r)

        logos = []
        for x in wData:
            if 'websites' in x:
                for y in x['websites']:
                    if y in logos:
                        pass
                    else:
                        logos.append(y)
        print(logos)

def collectParent_Company():

    with open('Companies.json', 'r') as r:
        wData = json.load(r)

        parent = []
        for x in wData:
            if 'parent' in x:
                if x['parent'] in parent:
                    pass
                else:
                    parent.append(x['parent'])
        print(parent)



def processData():
    processedData = []
    with open('Companies.json','r') as r:
        pData = json.load(r)

        for x in pData:
            record = []
            # name
            record.append((x['name']))

            # start date
            try:
                date = datetime.datetime.fromtimestamp(x['start_date'])
                record.append((date.strftime('%Y-%m-%d')))
            except:
                record.append('N/A')

            # website
            if 'websites' in x:
                with open("Company_Website.json", 'r') as cw:
                    cwData = json.load(cw)
                    websites = []
                    for cws in cwData:
                        for w in x['websites']:
                            if cws['id'] == w:
                                websites.append(cws['url'])
                    cw.close()
                    record.append(websites)
            else:
                record.append("N/A")

            # parent company
            if 'parent' in x:
                with open("Parent_Company.json", 'r') as pc:
                    pcData = json.load(pc)
                    parent = 'N/A'


                    for pcs in pcData:
                        if x['parent'] == pcs['id']:
                            parent = (pcs['name'])
                pc.close()
                record.append(parent)
            else:
                record.append('N/A')

            # description
            d = 'N/A'
            if 'description' in x:
                d = (x['description']).replace("\n\n", "")
                d = (d.replace("\n \n", ''))
                record.append(d)
            else:
                record.append(d)

            # logo
            if 'logo' in x:
                with open('Company_Logos.json', 'r') as cl:
                    clogos = json.load(cl)
                    for l in clogos:
                        if l['id'] == x['logo']:
                            record.append(l['url'])
                            cl.close()
            else:
                record.append("N/A")


            titleids = []
            title = []
            if 'developed' in x:
                titleids = x['developed']
            if 'published' in x:
                titleids = titleids + x['published']
            if len(titleids)>0:
                with open("Game_Titles.json", 'r') as gt:
                    gtData = json.load(gt)
                    title_platformsids = []

                    for t in titleids:
                        for gts in gtData:
                            if t== gts['id']:
                                title.append(gts['name'])
                                title_platformsids.append(gts['platforms'])
                    record.append(title)
                    gt.close()

                title_platformsids = sorted(list(dict.fromkeys([j for i in title_platformsids for j in i])))


                platforms = []
                with open("Platforms.json", 'r') as pl:
                    plData = json.load(pl)
                    for tids in title_platformsids:
                        for pls in plData:
                            if tids == pls['id']:
                                platforms.append(pls['name'])
                record.append(platforms)
            else:
                record.append("N/A")
                record.append("N/A")

            record.append(x['id'])

            processedData.append(record)

    return processedData

def buildCSV(listy):
    with open("FinalGameCompanies.csv", 'a+') as f:
        for x in range(len(listy)):
            csv_writer = writer(f)
            csv_writer.writerow(listy[x])
        f.close()

def main():

    buildCSV(processData())

main()