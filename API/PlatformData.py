import json
import datetime
from csv import writer


def collectWebsites():

    with open('Platforms.json', 'r') as pl:
        plData = json.load(pl)

        websites = []
        for x in plData:
            if 'websites' in x:
                for y in x['websites']:
                    websites.append(y)

        print(websites)

def collectLogo():
    with open('Platforms.json', 'r') as pl:
        plData = json.load(pl)

        logos = []
        for x in plData:
            if 'platform_logo' in x:
                logos.append(x['platform_logo'])

        print(logos)



def collectVersion():
    with open('Platforms.json','r') as vd:
        vdData = json.load(vd)

        versions = []
        for x in vdData:
            for y in x['versions']:
                versions.append(y)
        print(versions)

def collectVersionReleaseDate():
    with open('Platform_Versions.json','r') as pv:
        pvData = json.load(pv)

        release_date = []
        for x in pvData:
            if 'platform_version_release_dates' in x.keys():
                for y in x['platform_version_release_dates']:
                    release_date.append(y)

        print(release_date)



def processData():

    processedData = []

    with open('Platforms.json','r') as pl:
        plData = json.load(pl)

        for x in plData:
            record = []

            # name
            record.append(x['name'])

            # time
            with open("Platform_Versions.json") as pv:
                pvData = json.load(pv)

                platformID = None
                for pvs in pvData:
                    if pvs['id'] == x['versions'][0]:
                        if ('platform_version_release_dates' in pvs):
                            platformID = pvs['platform_version_release_dates'][0]

                pv.close()

            with open("Platform_Version_Release_Dates.json") as pvrd:
                pvrdData = json.load(pvrd)

                releaseDate = None
                for pvrds in pvrdData:
                    if pvrds['id'] == platformID:
                        releaseDate = pvrds['human']
                pvrd.close()
            record.append(releaseDate)


            # website
            if 'websites' in x:
                with open('Platform_Websites.json','r') as pw:
                    pwData = json.load(pw)
                    websites = ''

                    for y in x['websites']:
                        for z in pwData:
                            if y == z['id']:
                                websites = websites + z['url']
                    pw.close()
                record.append(websites)
            else:
                record.append("N/A")

            # category
            categories = ['Console','Arcade','Platform','Operating System','Portable Console', 'Computer']
            record.append(categories[(x['category'])-1])

            # summary
            if 'summary' in x:
                record.append(x['summary'])
            else:
                record.append("N/A")

            if 'platform_logo' in x:
                with open("Platform_Logos.json", 'r') as pl:
                    plData = json.load(pl)
                    logo = "N/A"
                    for pls in plData:
                        if pls['id'] == x['platform_logo']:
                            logo = pls['url']
                    pl.close()
                record.append(logo)
            else:
                record.append("N/A")

            # game titles
            with open("Game_Titles.json", 'r') as gt:
                gtData = json.load(gt)
                titles = []
                involved_companies = []
                for gts in gtData:
                    for t in gts['platforms']:
                        if t == x['id']:
                            titles.append(gts['name'])
                            involved_companies.append(gts['involved_companies'])
                gt.close()
            record.append(titles)

            # companies
            involved_companies = involved_companies[0]
            with open("Involved_Companies.json", 'r') as ic:
                icData = json.load(ic)
                companyIDs = []
                for y in involved_companies:
                    for ics in icData:
                        if ics['id'] == y:
                            companyIDs.append(ics['company'])
                ic.close()

            with open("Companies.json",'r') as c:
                cData = json.load(c)
                companies = []
                for y in companyIDs:
                    for cs in cData:
                        if y == cs['id']:
                            companies.append(cs['name'])
                c.close()
            record.append(companies)

            # id
            record.append(x['id'])


            processedData.append(record)
    return processedData

def FinalDataCSV(listy):

    for x in range(len(listy)):
        with open("FinalGamePlatforms.csv", 'a+') as f:
            csv_writer = writer(f)
            csv_writer.writerow(listy[x])

def main():


    a = processData()

    for x in a:
        print(x)




main()