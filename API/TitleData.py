import requests
import json
from csv import writer
import csv
from colorama import Fore
from colorama import init
init(autoreset=True)


def gamesSearchAPI(term):
    parameters = {
        'search': term,
        'page_size': '2'
    }

    response = requests.get('https://api.rawg.io/api/games', params=parameters)

    print(response.status_code)
    print(response.json().keys())
    final = []

    for x in range(len(response.json()['results'])):

        # extract Name
        name = (response.json()['results'][x]['name'])
        print("Name: " + name)

        # extract Genres
        genre = []
        for a in range(len(response.json()['results'][x]['genres'])):
            genre.append(response.json()['results'][x]['genres'][a]['name'])
        print("--Genre: " + str(genre))

        # extract released date
        released = (response.json()['results'][x]['released'])[0:4]
        print("--Released: " + released[0:4])

        # extract rating
        rating = (response.json()['results'][x]['rating'])
        print("--Rating: " + str(rating))

        # extract platforms
        platforms = []
        for a in range(len(response.json()['results'][x]['platforms'])):
            platforms.append(response.json()['results'][x]['platforms'][a]['platform']['name'])
        print("--Platforms: " + str(platforms))

        # extract id
        id = (response.json()['results'][x]['id'])
        print("--ID: " + str(id))

        # extract image
        image = (response.json()['results'][x]['background_image'])
        print("--Image: " + str(image))

        secondary = (requests.get('https://api.rawg.io/api/games/' + str(id))).json()

        website = secondary['website']
        print("--Website: " + str(website))

        developer = secondary['developers']
        developers = []
        for a in range(len(developer)):
            developers.append(developer[a]['name'])
        print("--Publishers:" + str(developers))

        listy = [name, ', '.join(genre), released, rating, ', '.join(platforms), ', '.join(developers), website, image,
                 id]
        final.append(listy)

    return final

def GameDataAPI():
    header = {
        'user-key': "ef5242c4eac8815a7250dab10cca8f2a"
    }
    body = "fields *; where id = (1942, 7348, 991, 6803, 989, 987, 986, 621, 119160, 949, 545, 119177, 1122, 9509, 2033," \
           " 1020, 731, 730, 819, 980, 1558, 1559, 1561, 18100, 1518, 8284, 71, 72, 3065, 11360, 254, 331, 235, 73, 74, " \
           "75, 231, 233, 2155, 2368, 11133, 2655, 2645, 2687, 2688, 109462, 141, 142, 76722, 26401, 498, 1011, 19164);" \
           " limit 60;"

    response = requests.get('https://api-v3.igdb.com/games', headers=header, data=body)

    print(response.status_code)
    print(response.json())

def readData():

    with open('Game_Titles.json', 'r') as j:
        data = json.load(j)
        game_data = []

        for x in range(len(data)):
            item = []

            #print(str(x) + " " + data[x]['name'] + ' ' + str(data[x]['id']))
            name = data[x]['name']
            item.append(name)

            # genres
            try:
                # print(data[x]['genres'])
                genres = data[x]['genres']
            except:
                # print(Fore.RED +data[x]['name'] + " has no age rating")
                genres = 'n/a'
            item.append(genres)

            # release_dates
            try:
                # print(data[x]['release_dates'])
                release_dates = data[x]['release_dates']
            except:
                # print(Fore.RED + data[x]['name'] + "has no release dates")
                release_dates = 'n/a'
            item.append(release_dates)

            # aggregated_rating
            try:
                #print(data[x]['aggregated_rating'])
                aggregated_rating = data[x]['aggregated_rating']
            except:
                #print(Fore.RED + data[x]['name'] + " has no aggregated rating")
                aggregated_rating = 'n/a'
            item.append(aggregated_rating)

            # age_ratings
            try:
                # print(data[x]['age_ratings'])
                age_ratings = data[x]['age_ratings']
            except:
                # print(Fore.RED +data[x]['name'] + " has no age rating")
                age_ratings = 'n/a'
            item.append(age_ratings)

            # platforms
            try:
                #print(data[x]['platforms'])
                platforms = data[x]['platforms']
            except:
                #print(Fore.RED + data[x]['name'] + "has no platforms")
                platforms = 'n/a'
            item.append(platforms)

            # involved_companies
            try:
                #print(data[x]['involved_companies'])
                involved_companies = data[x]['involved_companies']
            except:
                #print(Fore.RED + data[x]['name'] + "has no involved companies")
                involved_companies = 'n/a'
            item.append(involved_companies)

            # cover
            try:
                # print(data[x]['cover'])
                cover = data[x]['cover']
            except:
                # print(Fore.RED + data[x]['name'] + "has no involved companies")
                cover = 'n/a'
            item.append(cover)

            item.append(data[x]['id'])
            game_data.append(item)

    #for x in game_data:
        #print(x)

    return game_data

def GameDataCSV(listy):
    for x in range(len(listy)):
        with open("GameData.csv", 'a+') as f:
            csv_writer = writer(f)
            csv_writer.writerow(listy[x])

def collectGenres():

    trial = readData()

    genres = []
    for x in range(len(trial)):
        for g in trial[x][1]:
            if g in genres:
                pass
            else:
                genres.append(g)
    print(genres)

def collectRelease():
    trial = readData()

    releases = []
    for x in range(len(trial)):
        for r in trial[x][2]:
            if r in releases:
                pass
            else:
                releases.append(r)
    print((releases))

def collectAgeRating():
    trial = readData()

    age_rating = []
    for x in range(len(trial)):
        for ar in trial[x][4]:
            if ar in age_rating:
                pass
            else:
                age_rating.append(ar)
    print(age_rating)

def collectPlatforms():
    trial = readData()

    platforms = []
    for x in range(len(trial)):
        for p in trial[x][5]:
            if p in platforms:
                pass
            else:
                platforms.append(p)
    print(platforms)

def collectCompanies():
    involved_companies = []
    with open('Game_Titles.json', 'r') as gt:
        gtData = json.load(gt)
        for x in gtData:
            involved_companies.append(x['involved_companies'])
            for y in x['involved_companies']:
                if y == 42019:
                    print (x['name'])
        involved_companies = sorted(list(dict.fromkeys([j for i in involved_companies for j in i])))
        gt.close()
    print(involved_companies)

    with open('Involved_Companies.json') as ic:
        icData = json.load(ic)
        companies = []
        for x in range(len(icData)):
            if icData[x]['company'] in companies:
                pass
            else:
                companies.append(icData[x]['company'])
        ic.close()
    print(sorted(companies))

def collectCover():
    trial = readData()

    cover = []
    for x in trial:
        if x[7] in cover:
            pass
        else:
            cover.append(x[7])
    print(cover)



def finalData():
    preprocess = readData()
    postprocess = []

    for x in range(len(preprocess)):
        record = []

        # name
        record.append(preprocess[x][0])

        # genre
        genres = []
        with open('Genres.json', 'r') as g:
            gData = json.load(g)
            for y in preprocess[x][1]:
                for gs in gData:
                    if gs['id'] == y:
                        genres.append(gs['name'])
            g.close()
        record.append(genres)

        # release_dates
        releaseDates = []
        with open('Release_Dates.json', 'r') as r:
            rData = json.load(r)
            for y in preprocess[x][2]:
                for rs in rData:
                    if rs['id'] == y:
                        releaseDates.append(rs['human'])
            r.close()
        record.append(releaseDates[0])

        # rating
        try:
            record.append(int(preprocess[x][3]))
        except:
            record.append(preprocess[x][3])

        # age_rating
        age_rating = []
        table = ['Three','Seven','Twelve','Sixteen','Eighteen',
                'RP','EC','E','E10','T','M','AO']
        with open('Age_Rating.json', 'r') as ar:
            arData = json.load(ar)
            for y in preprocess[x][4]:
                for ars in arData:
                    if ars['id'] == y:
                        age_rating.append(ars['rating'])
            r.close()
        record.append(table[max(age_rating)-1])

        # platforms
        platforms = []
        with open('Platforms.json', 'r') as p:
            pData = json.load(p)
            for y in preprocess[x][5]:
                for ps in pData:
                    if ps['id'] == y:
                        platforms.append(ps['name'])
            p.close()
        record.append(platforms)

        # companies
        companies = []
        with open('Involved_Companies.json', 'r') as ic:
            icData = json.load(ic)
            for y in preprocess[x][6]:
                for ics in icData:
                    if ics['id'] == y:
                        ref = ics['company']
                        with open('Companies.json','r') as c:
                            cData = json.load(c)
                            for z in cData:
                                if z['id'] == ref:
                                    companies.append(z['name'])
                        c.close()
        ic.close()
        record.append(companies)

        # platforms
        with open('Cover.json', 'r') as co:
            coData = json.load(co)
            y = preprocess[x][7]
            for cos in coData:
                if cos['id'] == y:
                    record.append(cos['url'])
            co.close()

        record.append(preprocess[x][8])

        #print(record)
        postprocess.append(record)

    return postprocess

def FinalDataCSV(listy):
    for x in range(len(listy)):
        with open("FinalGameTitles.csv", 'a+') as f:
            csv_writer = writer(f)
            csv_writer.writerow(listy[x])

def main():

    collectCompanies()

main()