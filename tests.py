import os
import sys
import unittest
from create_db import app, db, Game, Company, Platform, Age_Rating, Genre, Version, create_db

class DBTestCases(unittest.TestCase):
    def test_game_is_in_db(self):
        tester = db.session.query(Game).filter_by(id = 71).first()

        self.assertEqual(tester.id, 71)
        self.assertEqual(tester.name, "Portal")
        self.assertEqual(tester.rating, "87.63")
        self.assertEqual(tester.cover_url, "//images.igdb.com/igdb/image/upload/t_thumb/co1x7d.jpg")

        ntester = db.session.query(Game).filter_by(id = 109462).first()

        self.assertEqual(ntester.rating, None)

    def test_agerating_is_in_db(self):
        tester = db.session.query(Age_Rating).filter_by(id = 12749).first()

        self.assertEqual(int(tester.id), 12749)
        self.assertEqual(str(tester.rating), "Eighteen")
        self.assertEqual(tester.cover_url, None)

    def test_genre_is_in_db(self):
        tester = db.session.query(Genre).filter_by(id = 31).first()

        self.assertEqual(int(tester.id), 31)
        self.assertEqual(str(tester.name), "Adventure")

    def test_company_is_in_db(self):
        tester = db.session.query(Company).filter_by(id = 823).first()

        self.assertEqual(int(tester.id), 823)
        self.assertEqual(str(tester.name), "United Front Games")
        self.assertEqual(str(tester.url), "http://www.unitedfrontgames.com/")

        ntester = db.session.query(Company).filter_by(id = 1438).first()

        self.assertEqual(ntester.logo_url, None)

    def test_platform_is_in_db(self):
        tester = db.session.query(Platform).filter_by(id = 8).first()

        self.assertEqual(int(tester.id), 8)
        self.assertEqual(str(tester.name), "PlayStation 2")
        self.assertEqual(tester.summary, None)
        self.assertEqual(str(tester.logo_url), "//images.igdb.com/igdb/image/upload/t_thumb/pl72.jpg")

    def test_version_is_in_db(self):
        tester = db.session.query(Version).filter_by(id = 147).first()

        self.assertEqual(int(tester.id), 147)
        self.assertEqual(str(tester.name), "Lion")

        
if __name__ == '__main__':
    unittest.main()
